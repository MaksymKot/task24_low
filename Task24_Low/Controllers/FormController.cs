﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;

namespace Task24_Low.Controllers
{
    public class FormController : Controller
    {
        [HttpGet]
        public ActionResult Form()
        {
            return View("FormPage");
        }

        [HttpPost]
        public ActionResult FormPage(FormData data)
        {
            if (String.IsNullOrEmpty(data.LevelOfService))
                ModelState.AddModelError("LevelOfService", "Please choise a level of service");
            
            if (String.IsNullOrEmpty(data.PrimaryUsing))
                ModelState.AddModelError("PrimaryUsing", "Please tell about your primary using of this library");
            
            if (String.IsNullOrEmpty(data.Satisfaction))
                ModelState.AddModelError("Satisfaction", "Please choise your satisfaction level");
            
            if (String.IsNullOrEmpty(data.Visit))
                ModelState.AddModelError("Visit", "Please say how often do you visit library");
            
            if (ModelState.IsValid)
            {
                (HttpContext.Application["forms"] as List<FormData>)
                .Add(data);
                return RedirectToAction("FormPage");
            }
            else
                return View("FormPage");
            
        }

        [HttpGet]
        public ActionResult FormPage()
        {
            return View("FormPageResult", HttpContext.Application["forms"] );
        }
    }
}