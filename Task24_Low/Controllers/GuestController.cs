﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;

namespace Task23_Middle.Controllers
{
    public class GuestController : Controller
    {
        [HttpGet]
        public ActionResult GuestPage()
        {
            return View(HttpContext.Application["reviews"]);
        }

        [HttpPost]
        public ActionResult AddReview(Review review)
        {
            if (string.IsNullOrEmpty(review.AuthorName) && 
                    string.IsNullOrEmpty(review.Text))
            {
                ModelState.AddModelError(
                    "", "Please enter your name and your review");
            }
            else if (!string.IsNullOrEmpty(review.AuthorName) &&
                    string.IsNullOrEmpty(review.Text))
            {
                ModelState.AddModelError("", "Please enter your review");
            } 
            else if (string.IsNullOrEmpty(review.AuthorName) &&
                    !string.IsNullOrEmpty(review.Text))
            {
                ModelState.AddModelError("", "Please enter your name");
            }

            if (ModelState.IsValid)
            {
                (HttpContext.Application["reviews"] as List<Review>)
                    .Add(review);
                return new RedirectResult("/Guest/GuestPage");
            }
            else
            {
                return View("GuestPage", HttpContext.Application["reviews"]);
            }
        }
    }
}
