﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;

namespace Task24_Low.ExtensionMethods
{
    public static class DrawResultsOFForm
    {
        public static MvcHtmlString DrawResults(this HtmlHelper html,
            List<FormData> data)
        {
            var h2 = new TagBuilder("h2");
            h2.SetInnerText("Result of form");
            h2.Attributes.Add("class", "text-center");
            
            var visits = new Dictionary<string,int>();

            visits["Monthly"] = data.Count(x => x.Visit.Equals("Monthly"));
            visits["Weekly"] = data.Count(x => x.Visit.Equals("Weekly"));
            visits["Daily"] = data.Count(x => x.Visit.Equals("Daily"));

            var vists = new TagBuilder("h3");
            vists.SetInnerText("How often people visit the library");

            h2.InnerHtml += vists;

            var visitList = new TagBuilder("ul");

            foreach (var a in visits)
            {
                var li = new TagBuilder("li");
                li.SetInnerText($"{a.Key}: {a.Value}");
                visitList.InnerHtml += li;
            }

            h2.InnerHtml += visitList;

            // dictionary, which keep info about visit options
            var satisf = new Dictionary<string, int>();

            satisf["Satisfied"] = data.Count(x => x.Satisfaction.Equals("Satisfied"));
            satisf["Indiffirent"] = data.Count(x => x.Satisfaction.Equals("Indiffirent"));
            satisf["Dissatisfied"] = data.Count(x => x.Satisfaction.Equals("Dissatisfied"));

            var satsf = new TagBuilder("h3");
            satsf.SetInnerText("Rate of service");

            h2.InnerHtml += satsf;

            // dictionary, which keep info about satisfaction options
            var satisfList = new TagBuilder("ul");

            foreach (var a in satisf)
            {
                var li = new TagBuilder("li");
                li.SetInnerText($"{a.Key}: {a.Value}");
                satisfList.InnerHtml += li;
            }

            h2.InnerHtml += satisfList;

            // dictionary, which keep info about primary using options
            var primUsing = new Dictionary<string, int>();

            primUsing["Leisure"] = data.Count(x => x.PrimaryUsing.Equals("Leisure"));
            primUsing["Education"] = data.Count(x => x.PrimaryUsing.Equals("Education"));
            primUsing["Work"] = data.Count(x => x.PrimaryUsing.Equals("Work"));

            var primUse = new TagBuilder("h3");
            primUse.SetInnerText("Primarily use the library and its services");

            h2.InnerHtml += primUse;

            var useList = new TagBuilder("ul");

            foreach (var a in primUsing)
            {
                var li = new TagBuilder("li");
                li.SetInnerText($"{a.Key}: {a.Value}");
                useList.InnerHtml += li;
            }

            h2.InnerHtml += useList;

            // dictionary, which keep info about level of service options
            var lvlServ = new Dictionary<string, int>();

            lvlServ["Excelent"] = data.Count(x => x.LevelOfService.Equals("Excelent"));
            lvlServ["Very good"] = data.Count(x => x.LevelOfService.Equals("Very good"));
            lvlServ["Sufficient"] = data.Count(x => x.LevelOfService.Equals("Sufficient"));
            lvlServ["Insufficient"] = data.Count(x => x.LevelOfService.Equals("Insufficient"));

            var serv = new TagBuilder("h3");
            serv.SetInnerText("Level of service:");

            h2.InnerHtml += serv;

            var servList = new TagBuilder("ul");

            foreach(var a in lvlServ)
            {
                var li = new TagBuilder("li");
                li.SetInnerText($"{a.Key}: {a.Value}");
                servList.InnerHtml += li;
            }

            h2.InnerHtml += servList;
            
            return new MvcHtmlString(h2.ToString());
        }
    }
}
